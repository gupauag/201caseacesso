package com.br.Acesso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationKafkaConsumer {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationKafkaConsumer.class, args);
	}

}
