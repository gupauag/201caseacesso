package com.br.Acesso.Consumer;

import com.br.Acesso.Service.Kafka.DTO.LogAcesso;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
public class LogAcessoConsumer {

    @KafkaListener(topics = "spec3-gustavo-aguiar-1", groupId = "GustavoAraujo-1")

    public void receber(@Payload LogAcesso logAcesso) throws Exception {
        System.out.println("Cliente id: " + logAcesso.getClienteId());
        System.out.println("Porta id: " + logAcesso.getPortaId());
        System.out.println("Horario: " + logAcesso.getDtSolicitacao());
        System.out.println("Permissao: " + logAcesso.isPermissao());
        montaArquivoCSV(logAcesso);
    }

    public void montaArquivoCSV(LogAcesso logAcesso) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        Writer writer = new FileWriter("/home/a2w/workspace/201CaseAcesso/LogAcesso.csv", true );
        StatefulBeanToCsv<LogAcesso> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(logAcesso);

        writer.flush();
        writer.close();

    }

}
