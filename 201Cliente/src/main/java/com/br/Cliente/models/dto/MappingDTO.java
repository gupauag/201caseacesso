package com.br.Cliente.models.dto;


import com.br.Cliente.models.Cliente;

public class MappingDTO {

    public Cliente mappingCliente(ClienteRequest clienteRequest){

        Cliente cliente = new Cliente();
        cliente.setId(clienteRequest.getId());
        cliente.setName(clienteRequest.getName());
        return cliente;
    }

}
