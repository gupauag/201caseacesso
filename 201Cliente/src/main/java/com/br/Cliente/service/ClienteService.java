package com.br.Cliente.service;



import com.br.Cliente.exception.ClienteNotFoundException;
import com.br.Cliente.models.Cliente;
import com.br.Cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente consultarClientePorId(int id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        if(optionalCliente.isPresent())
            return optionalCliente.get();
        throw new ClienteNotFoundException();
    }
}
