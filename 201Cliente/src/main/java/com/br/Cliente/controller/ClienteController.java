package com.br.Cliente.controller;


import com.br.Cliente.models.Cliente;
import com.br.Cliente.models.dto.ClienteRequest;
import com.br.Cliente.models.dto.MappingDTO;
import com.br.Cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    MappingDTO mappingDTO = new MappingDTO();

    @PostMapping
    public ResponseEntity<Cliente> criarCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.criarCliente(cliente);
        return ResponseEntity.status(201).body(clienteObjeto);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Cliente> consultarClientePorId(@PathVariable(name = "id") int id){
//        System.out.println("ID: " +id);
//        System.out.println("Recebi uma requisição! " + System.currentTimeMillis());
        Cliente clienteObjeto = clienteService.consultarClientePorId(id);
        return ResponseEntity.status(200).body(clienteObjeto);
    }


}
