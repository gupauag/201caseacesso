package com.br.Porta.Controller;

import com.br.Porta.Model.Porta;
import com.br.Porta.Service.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    public ResponseEntity<Porta> criarPorta(@RequestBody Porta porta){
        return ResponseEntity.status(201).body(portaService.criarPorta(porta));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Porta> getPorta(@PathVariable(name = "id") int id){
        return ResponseEntity.status(200).body(portaService.getPorta(id));
    }

}
