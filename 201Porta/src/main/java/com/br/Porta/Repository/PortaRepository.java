package com.br.Porta.Repository;

import com.br.Porta.Model.Porta;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PortaRepository extends CrudRepository<Porta, Integer> {

    Optional<Porta> findByAndarAndSala(String andar, String sala);

}
