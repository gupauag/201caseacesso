package com.br.Porta.Service;

import com.br.Porta.Exception.PortaExistenteException;
import com.br.Porta.Exception.PortaNotFoundException;
import com.br.Porta.Model.Porta;
import com.br.Porta.Repository.PortaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PortaService {

    @Autowired
    private PortaRepository portaRepository;

    public Porta criarPorta (Porta porta){

        Optional<Porta> portaOptional = portaRepository.findByAndarAndSala(porta.getAndar(),porta.getSala());
        if(portaOptional.isPresent())
            throw new PortaExistenteException();
        return portaRepository.save(porta);
    }

    public Porta getPorta(int idPorta){
        Optional<Porta> portaOptional = portaRepository.findById(idPorta);
        if(portaOptional.isPresent()) return portaOptional.get();
        throw new PortaNotFoundException();
    }

}
