package com.br.Porta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationPorta {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationPorta.class, args);
	}

}
