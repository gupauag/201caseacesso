package com.br.Porta.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Porta e Andar, já cadastrados.")
public class PortaExistenteException extends RuntimeException {
}

