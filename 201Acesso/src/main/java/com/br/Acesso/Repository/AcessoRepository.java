package com.br.Acesso.Repository;

import com.br.Acesso.Model.Acesso;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {

    @Query(value = "select * from acesso where cliente_id = :clienteId and porta_id = :portaId", nativeQuery = true)
    Optional<Acesso> findByClienteIdAndPortaId(@Param("clienteId") int clienteId, @Param("portaId") int portaId);

    @Modifying
    @Transactional
    @Query(value = "delete from acesso where cliente_id = :clienteId and porta_id = :portaId", nativeQuery = true)
    void deleteAcesso(@Param("clienteId") int clienteId, @Param("portaId") int portaId);

}
