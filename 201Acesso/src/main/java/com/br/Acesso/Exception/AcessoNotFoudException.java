package com.br.Acesso.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Acesso não encontrado.")
public class AcessoNotFoudException extends RuntimeException {
}

