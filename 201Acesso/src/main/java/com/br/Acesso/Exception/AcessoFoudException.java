package com.br.Acesso.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Acesso já cadastrado para esse cliente e porta.")
public class AcessoFoudException extends RuntimeException {
}

