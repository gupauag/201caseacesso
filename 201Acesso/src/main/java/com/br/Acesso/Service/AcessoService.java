package com.br.Acesso.Service;

import com.br.Acesso.Client.Cliente.ClienteClient;
import com.br.Acesso.Client.Cliente.DTO.ClienteResponse;
import com.br.Acesso.Client.Porta.DTO.PortaResponse;
import com.br.Acesso.Client.Porta.PortaClient;
import com.br.Acesso.Exception.AcessoFoudException;
import com.br.Acesso.Exception.AcessoNotFoudException;
import com.br.Acesso.Model.Acesso;
import com.br.Acesso.Repository.AcessoRepository;
import com.br.Acesso.Service.Kafka.DTO.LogAcesso;
import com.br.Acesso.Service.Kafka.LogAcessoProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private LogAcessoProducer logAcessoProducer;


    public Acesso criarAcesso(Acesso acesso){

        //Um acesso só pode ser criado se o usuário for válido.
        Optional<ClienteResponse> clienteResponse = clienteClient.consultarClientePorId(acesso.getCliente_Id());

        //Um acesso só pode ser criado se a porta for válida.
        Optional<PortaResponse> portaResponse = portaClient.getPorta(acesso.getPorta_Id());

        //Valida se já esta cadastrado
        Optional<Acesso> acessoOptional = acessoRepository.findByClienteIdAndPortaId(acesso.getCliente_Id(),
                acesso.getPorta_Id());
        if(acessoOptional.isPresent()) throw new AcessoFoudException();

        return acessoRepository.save(acesso);
    }

    public Acesso getAcesso(int clienteId, int portaId){
        Optional<Acesso> acessoOptional = acessoRepository.findByClienteIdAndPortaId(clienteId,portaId);
        if(acessoOptional.isPresent()) {

            LogAcesso logAcesso = new LogAcesso(clienteId,portaId,new Date(),true);
            logAcessoProducer.enviarAoKafka(logAcesso);

            return acessoOptional.get();
        }
        else{
            LogAcesso logAcesso = new LogAcesso(clienteId,portaId,new Date(),false);
            logAcessoProducer.enviarAoKafka(logAcesso);

            throw new AcessoNotFoudException();
        }


    }

    public void deleteAcesso(int clienteId, int portaId){
        acessoRepository.deleteAcesso(clienteId,portaId);
    }

}
