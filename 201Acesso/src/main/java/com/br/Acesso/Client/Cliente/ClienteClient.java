package com.br.Acesso.Client.Cliente;

import com.br.Acesso.Client.Cliente.DTO.ClienteResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    Optional<ClienteResponse> consultarClientePorId(@PathVariable int id);

}
