package com.br.Acesso.Client.Cliente;

import com.br.Acesso.Client.Cliente.Exception.ClienteNotFoundException;
import com.br.Acesso.Client.Cliente.Exception.InvalidClienteException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            throw new ClienteNotFoundException();
        }else if(response.status() == 404){
            throw new InvalidClienteException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }

}
