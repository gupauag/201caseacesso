package com.br.Acesso.Client.Porta;

import com.br.Acesso.Client.Cliente.ClienteClientConfiguration;
import com.br.Acesso.Client.Porta.DTO.PortaResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)
public interface PortaClient {

    @GetMapping("/porta/{id}")
    Optional<PortaResponse> getPorta(@PathVariable int id);

}
