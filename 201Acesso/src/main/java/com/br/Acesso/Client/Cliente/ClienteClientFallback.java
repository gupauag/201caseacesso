package com.br.Acesso.Client.Cliente;


import com.br.Acesso.Client.Cliente.DTO.ClienteResponse;
import com.br.Acesso.Client.Cliente.Exception.OffilineClienteException;

import java.util.Optional;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Optional<ClienteResponse> consultarClientePorId(int id){
        throw new OffilineClienteException();
    }

}
