package com.br.Acesso.Client.Porta.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "A porta informado é inválida")
public class InvalidPortaException extends RuntimeException {
}
