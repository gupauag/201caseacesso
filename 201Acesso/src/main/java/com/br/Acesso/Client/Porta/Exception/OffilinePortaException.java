package com.br.Acesso.Client.Porta.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O serviço de porta se encontra offiline")
public class OffilinePortaException extends RuntimeException {
}
