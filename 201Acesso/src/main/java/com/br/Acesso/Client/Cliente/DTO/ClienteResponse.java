package com.br.Acesso.Client.Cliente.DTO;

public class ClienteResponse {
    private int id;

    private String name;

    public ClienteResponse() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
