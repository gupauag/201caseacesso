package com.br.Acesso.Client.Porta;

import com.br.Acesso.Client.Porta.DTO.PortaResponse;
import com.br.Acesso.Client.Porta.Exception.OffilinePortaException;

import java.util.Optional;

public class PortaClientFallback implements PortaClient {

    @Override
    public Optional<PortaResponse> getPorta(int id){
        throw new OffilinePortaException();
    }

}
