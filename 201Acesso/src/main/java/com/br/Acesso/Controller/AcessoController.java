package com.br.Acesso.Controller;

import com.br.Acesso.Model.Acesso;
import com.br.Acesso.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @PostMapping
    public ResponseEntity<Acesso> criarAcesso(@RequestBody Acesso acesso){
        return ResponseEntity.status(201).body(acessoService.criarAcesso(acesso));
    }

    @GetMapping("/{cliente_id}/{porta_id}")    public ResponseEntity<Acesso> getAcesso(@PathVariable(name = "cliente_id") int cliente_id,
                                            @PathVariable(name = "porta_id") int porta_id ) {
        return ResponseEntity.status(200).body(acessoService.getAcesso(cliente_id,porta_id));
    }

    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAcesso(@PathVariable(name = "cliente_id") int cliente_id,
                                            @PathVariable(name = "porta_id") int portal_id ) {
        acessoService.deleteAcesso(cliente_id, portal_id);
    }

}
